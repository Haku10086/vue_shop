import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import "./plugins/element.js";
import axios from "axios";
// 导入字体图标
import "./assets/fonts/iconfont.css";
// 导入全局样式表
import "../src/assets/css/global.css";
import TreeTable from "vue-table-with-tree-grid";
import VueQuillEditor from "vue-quill-editor";
import "quill/dist/quill.core.css";
import "quill/dist/quill.snow.css";
import "quill/dist/quill.bubble.css";

import NProgress from "nprogress";
import "nprogress/nprogress.css";
import nProgress from "nprogress";

Vue.use(VueQuillEditor);
Vue.component("tree-table", TreeTable);
Vue.config.productionTip = false;

Vue.prototype.$http = axios;

// axios 配置请求的根路径
axios.defaults.baseURL = "http://127.0.0.1:8888/api/private/v1/";

// NOTE: 在 request 拦截器中，展示进度条 NProgress.start()
axios.interceptors.request.use(config => {
  NProgress.start();
  config.headers.Authorization = window.sessionStorage.getItem("token");
  // * 必须要在最后返回 config
  return config;
});

// NOTE: 在 response 拦截器中，隐藏进度条 NProgress.done()
axios.interceptors.response.use(config => {
  NProgress.done();
  return config;
});

// NOTE:日期过滤器
Vue.filter("dateFormat", function(originVal) {
  const dt = new Date(originVal);
  const years = dt.getFullYear();
  const month = (dt.getMonth() + 1 + "").padStart(2, "0");
  const day = (dt.getDay() + "").padStart(2, "0");
  const hour = (dt.getHours() + "").padStart(2, "0");
  const minute = (dt.getMinutes() + "").padStart(2, "0");
  const second = (dt.getSeconds() + "").padStart(2, "0");
  return `${years}-${month}-${day} ${hour}:${minute}:${second}`;
});

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
