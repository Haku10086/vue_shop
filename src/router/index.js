import Vue from "vue";
import VueRouter from "vue-router";
import Login from "../components/Login.vue";
import Home from "../components/Home.vue";
import Welcome from "../components/Welcome.vue";
import Users from "../components/Users.vue";
import Rights from "../components/Rights.vue";
import Roles from "../components/Roles.vue";
import Cate from "../components/Cate.vue";
import Params from "../components/Params.vue";
import Goods from "../components/Goods.vue";
import addPage from "../components/addPage.vue";
import Orders from "../components/Orders.vue";
import Report from "../components/Report.vue";

Vue.use(VueRouter);

const routes = [{
    path: "/login",
    component: Login
  },
  // 路由重定向
  {
    path: "/",
    redirect: "./login"
  },
  {
    path: "/home",
    component: Home,
    redirect: "/welcome",
    // 子路由
    children: [{
        path: "/welcome",
        component: Welcome
      },
      {
        path: "/users",
        component: Users
      },
      {
        path: "/rights",
        component: Rights
      },
      {
        path: "/roles",
        component: Roles
      },
      {
        path: "/categories",
        component: Cate
      },
      {
        path: "/params",
        component: Params
      },
      {
        path: "/goods",
        component: Goods
      },
      {
        path: "/goods/add",
        component: addPage
      },
      {
        path: "/orders",
        component: Orders
      },
      {
        path: "/reports",
        component: Report
      }
    ]
  }
];
const router = new VueRouter({
  routes
});

// 挂载路由导航守卫
router.beforeEach((to, from, next) => {
  // to 将要访问的路径
  // from 代表从那个路径跳转而来
  // next 是一个函数，表示放行
  if (to.path === "/login") {
    return next();
  }
  const tokenStr = window.sessionStorage.getItem("token");
  if (!tokenStr) {
    return next("./login");
  }
  next();
});

export default router;