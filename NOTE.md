## 技术要点(Think twice before code.)

1. 通过不同的前缀及后缀表示不同的变量类型(保持统一命名规则)
2. 写完代码请折叠，合理利用屏幕大小和编辑器的功能
3. 工程常用代码可放入代码块或专用文件保存，或者重构为函数
4. 注释也请分类表示(注释前也请空一行)

## 最佳实践(You build it you run it.)

1. 前端开发的接口使用必须和后端给的 API 文档一致，否则不能使用(注意空格)
2. 标签的属性默认值为 false 时，我们只要添加属性名就可以启动对应属性(如 单选框的 checked 属性)

## 实用代码(Talk is cheap show me the code.)

```Javascript
   // 日期过滤器 (样式 1970-01-00 20:37:39)
   Vue.filter("dateFormat", function(originVal) {
   const dt = new Date(originVal);
   const years = dt.getFullYear();
   const month = (dt.getMonth() + 1 + "").padStart(2, "0");
   const day = (dt.getDay() + "").padStart(2, "0");
   const hour = (dt.getHours() + "").padStart(2, "0");
   const minute = (dt.getMinutes() + "").padStart(2, "0");
   const second = (dt.getSeconds() + "").padStart(2, "0");
   return `${years}-${month}-${day} ${hour}:${minute}:${second}`;
   });

   // axios 配置请求的根路径
   axios.defaults.baseURL = "URL";
```

```CSS
    /* 盒模型在父元素居中关键代码(子绝父相): */
    * {
        position: absolute;
	    left: 50%;
	    top: 50%;
	    transform: translate(-50%, -50%);
    }
```
